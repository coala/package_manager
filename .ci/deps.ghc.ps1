Set-StrictMode -Version latest

function Sync-Cabal {
    $old_EAP = $ErrorActionPreference
    $ErrorActionPreference = 'Continue'

    cabal update

    $ErrorActionPreference = $old_EAP
}

function Complete-Install {
    Sync-Cabal
}
