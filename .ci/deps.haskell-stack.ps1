Set-StrictMode -Version latest

function Invoke-Stack-Setup {
    $old_EAP = $ErrorActionPreference
    $ErrorActionPreference = 'Continue'

    stack setup | grep -Ev '(Extracting|Extracted|%. downloaded)'

    $ErrorActionPreference = $old_EAP
}

function Complete-Install {
    Invoke-Stack-Setup
}

Export-ModuleMember -Function Stack-Setup
