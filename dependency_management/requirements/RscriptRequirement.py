import re
from sarge import run, Capture, get_stdout

from dependency_management.requirements.ExecutableRequirement import (
    ExecutableRequirement)
from dependency_management.requirements.PackageRequirement import (
    PackageRequirement)


VERSION_MATCHER = re.compile("\d+\.\d*\.?\d*")


class RscriptRequirement(PackageRequirement):
    """
    This class is a subclass of ``PackageRequirement``. It specifies the proper
    type for ``R`` packages automatically and provide a function to check
    for the requirement.
    """

    REQUIREMENTS = {ExecutableRequirement('R')}

    def __init__(self,
                 package,
                 version="",
                 flag="",
                 repo="http://cran.rstudio.com"):
        """
        Constructs a new ``RscriptRequirement``, using the
        ``PackageRequirement`` constructor.

        >>> pr = RscriptRequirement(
        ...         'formatR', version='',
        ...         repo="http://cran.rstudio.com")
        >>> pr.type
        'R'
        >>> pr.package
        'formatR'
        >>> pr.version
        ''
        >>> str(pr)
        'formatR'
        >>> pr.repo
        'http://cran.rstudio.com'

        :param package: A string with the name of the package to be installed.
        :param version: A version string. Leave empty to specify latest version.
        :param flag:    A string that specifies any additional flags, that
                        are passed to the type (WARNING: Deprecated).
        :param repo:    The repository from which the package is to be
                        installed.
        """
        PackageRequirement.__init__(self, 'R', package, version, repo)
        self.flag = flag

    def install_command(self):
        """
        Creates the installation command for the instance of the class.

        >>> RscriptRequirement(
        ...     'formatR', '' , '-e',
        ...     'http://cran.rstudio.com').install_command()
        ['R', '-e', '"install.packages(\\'formatR\\', repos=\\'http://cran.rstudio.com\\', dependencies=TRUE)"']

        >>> RscriptRequirement(
        ...     'formatR', '1.5', '-e',
        ...     'http://cran.rstudio.com').install_command()
        ['R', '-e', '"remotes::install_version(\\'formatR\\', repos=\\'http://cran.rstudio.com\\', version=\\'1.5\\', dependencies=TRUE)"']

        :param return: A list with the installation command.
        """
        if self.version is not "":
            install = "\"remotes::install_version('{}', " \
                        "repos='{}', " \
                        "version='{}', " \
                        "dependencies=TRUE)\""
            exact_version = VERSION_MATCHER.findall(self.version)[0]
        else:
            install = "\"install.packages('{}', " \
                        "repos='{}', " \
                        "dependencies=TRUE)\""
            exact_version = self.version
        result = ['R', '-e',
                  install.format(self.package, self.repo, exact_version)]
        return result

    def is_installed(self):
        """
        Checks if the dependency is installed.

        :param return: True if dependency is installed, false otherwise.
        """
        return not run(
                ('R -e \'library(\"{}\", quietly=TRUE)\''.format(self.package)),
                stdout=Capture(),
                stderr=Capture()).returncode

    def get_installed_version(self):
        if not self.is_installed():
            return None
        version_command = 'R -e "packageVersion(\'{}\')"'.format(self.package)
        output = get_stdout(version_command)
        return VERSION_MATCHER.findall(output)[-1]
